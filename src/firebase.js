import firebase from 'firebase/app'
import 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyApWyt6vHCZuc5lB8cQxpygX92tm5VjGic",
    authDomain: "firestore-react-11ee7.firebaseapp.com",
    projectId: "firestore-react-11ee7",
    storageBucket: "firestore-react-11ee7.appspot.com",
    messagingSenderId: "130655966109",
    appId: "1:130655966109:web:9465e0b4302252d3921574"
  };

  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  
  export const db = firebase.firestore();