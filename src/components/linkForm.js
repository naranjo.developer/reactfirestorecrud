import React , {useState , useEffect} from 'react';
import { db } from '../firebase';
import {toast} from 'react-toastify'

const LinkForm = (props) => {
    const initialState = {
        url: '',
        name: '',
        description: ''
    };
    const [values , setValues] = useState(initialState)

    const handleInputCHange = e =>{
        const {name , value} = e.target;
        setValues({...values , [name]: value})
    }
    
    const getLinkById = async (id) =>{
       const doc = await db.collection('links').doc(id).get();
        setValues({...doc.data()})
    }


    const validateURL = str =>{
            return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(str);

    }

    const handleSubmit = e =>{
        e.preventDefault();
        if(! validateURL(values.url)){
            return toast.error('Invalid URL', {
                position: "top-left",
                autoClose: 1500,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                });
        }else{
            props.addOrEditLink(values);
            setValues({...initialState})
        }
    };

    useEffect(() =>{
        if(props.currentId ===''){
            setValues({...initialState})
        }else{
            getLinkById(props.currentId)
        }
    } , [props.currentId]);

    return (
        <form className="card card-body" onSubmit={handleSubmit}>
            <div className="form-group input-group">
                <div className="input-group-text bg-light">
                    <i className="material-icons">insert_link</i>
                </div>
                <input value={values.url} onChange={handleInputCHange} type="text" className="form-control" placeholder="https://someurl.com" name="url" />
            </div>
            <div className="form-group input-group">
                <div className="input-group-text bg-light">
                    <i className="material-icons">create</i>
                </div>
                <input value={values.name} onChange={handleInputCHange} type="text" name="name" placeholder="Website Name" className="form-control" />
            </div>
            <div className="form-group">
                <textarea onChange={handleInputCHange} value={values.description} placeholder="Write the description" className="form-control" name="description" rows="3">
                </textarea>
            </div>
            <button className="btn btn-primary btn-block">
                {props.currentId==='' ? 'save' : 'Update '}
            </button>
        </form>
    )
}

export default LinkForm;