import React, { useEffect, useState } from 'react';
import LinkForm from './linkForm'
import { db } from '../firebase'
import { toast } from 'react-toastify';



const Links = () => {

    const [links, setLinks] = useState([]);
    const [currentId, setCurrentId] = useState('');

    const addOrEditLink = async (linkObject) => {
        try {
            if (currentId === '') {
                await db.collection('links').doc().set(linkObject);
                toast.dark(`🦄 Link created Successfully`, {
                    position: "bottom-center",
                    autoClose: 1500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            } else {
                await db.collection('links').doc(currentId).update(linkObject);
                toast.dark(`🦄 Link Updated Successfully`, {
                    position: "bottom-center",
                    autoClose: 1500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                setCurrentId('');
            }
        } catch (error) {
            console.error(error)
        }
    }


    const getLinks = () => {
        db.collection('links').onSnapshot((querySnapshot) => {
            const docs = [];
            querySnapshot.forEach(doc => {
                docs.push({ ...doc.data(), id: doc.id })
            })
            setLinks(docs);
        });
    }

    const onDeleteLInk = async (id) => {
        if (window.confirm('Are you sure to delete this link?')) {
            await db.collection('links').doc(id).delete();
            toast.error('Task Deleted!', {
                position: "bottom-center",
                autoClose: 1500,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
    }


    useEffect(() => {
        getLinks();
    }, []);

    return <div>
        <h3 className="text-center">Create Links</h3>

        <div className="col-md-4">
            <LinkForm {...{ addOrEditLink, currentId, links }} />

        </div>


        <div className="col-md-8 p-2">
            {links.map(link => (
                <div className="card mb-1 mt-2" key={link.id}>
                    <div className="card-body">
                        <div className="d-flex justify-content-between">
                            <h3> Link Name:  <strong className="text-primary"> {link.name} </strong> </h3>
                            <div>
                                <i onClick={() => setCurrentId(link.id)} className="material-icons ">create</i>
                                <i onClick={() => onDeleteLInk(link.id)} className="material-icons text-danger">close</i>
                            </div>

                        </div>
                        <h3> Link Description : <strong className="text-primary">{link.description}</strong> </h3>
                        <h3>Go to website <a href={link.url} rel="noreferrer" target="_blank"> {link.url} </a></h3>
                    </div>
                </div>
            ))}
        </div>
    </div>

}


export default Links;